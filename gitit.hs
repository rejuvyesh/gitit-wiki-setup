-- File: gitit.hs
-- Copyright rejuvyesh <mail@rejuvyesh.com>, 2014
-- License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>

import Network.Gitit
import Happstack.Server.SimpleHTTP
import qualified BibtexGitit as BG

main::IO()
main = do
  conf <- getConfigFromFile "my.conf"
  initializeGititState conf
  updateGititState (\s -> s { plugins = plugins s ++ [BG.plugin] })
  simpleHTTP nullConf{port = 9500} $ wiki conf
