function highlight(text, color) {
  document.body.innerHTML = document.body.innerHTML.replace(
    new RegExp(text + '(?!([^<]+)?>)', 'gi'),
    '<span style="background-color:'+ color +';font-size:100%">$&</span>'
  );
};
highlight("TODO", '#ff4d4d');

