#!/bin/zsh
#
# File: start.sh
#
# Created: Saturday, January 10 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

PATH=/usr/bin:/home/rejuvyesh/.nix-profile/bin
MATHJAX=/home/rejuvyesh/src/www/mathjax/MathJax/
WIKI=/home/rejuvyesh/src/www/wiki/
PAPERS=/home/rejuvyesh/papers/

# MathJax server
cd $MATHJAX
printf "Starting local mathjax server\n"
./hserv -p12000 &

# gitit
cd $WIKI
printf "Starting wiki\n"
./gitit 

# Papers
cd $PAPERS
printf "Serving papers"
./hserv -p9555 &
